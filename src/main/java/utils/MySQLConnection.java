package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySQLConnection {

    private Connection connection;

    private String ip;

    String database;

    String username;

    String password;

    public MySQLConnection() {
        this("trabajadores", "batoi", "1234");
    }

    public MySQLConnection(String ip, String database, String username, String password) {
        this.ip = ip;
        this.database = database;
        this.username = username;
        this.password = password;
    }

    public MySQLConnection(String database, String username, String password) {
        this.ip = "127.0.0.1";
        this.database = database;
        this.username = "root";
        this.password = "123456789";
    }

    public MySQLConnection(String database) {
        this.ip = "127.0.0.1";
        this.database = database;
        this.username = "root";
        this.password = "123456789";
    }

    public  Connection getConnection() {
        if(connection == null){
            String dbURL = "jdbc:mysql://" + ip + "/" + database + "?serverTimezone=UTC&allowPublicKeyRetrieval=true";
            try {
                connection = DriverManager.getConnection(dbURL,username,password);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        return connection;
    }


}
